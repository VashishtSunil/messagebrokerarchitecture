﻿namespace Microsoft.MessageBrokerArchitecture.Services.EventSource.Model;

public class CatalogType
{
    public int Id { get; set; }

    public string Type { get; set; }
}
