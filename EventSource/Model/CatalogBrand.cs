﻿namespace Microsoft.MessageBrokerArchitecture.Services.EventSource.Model;

public class CatalogBrand
{
    public int Id { get; set; }

    public string Brand { get; set; }
}
