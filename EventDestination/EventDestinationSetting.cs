﻿namespace Microsoft.MessageBrokerArchitecture.Services.EventSource;

public class EventDestinationSetting
{
    public string PicBaseUrl { get; set; }

    public string EventBusConnection { get; set; }

    public bool UseCustomizationData { get; set; }

    public bool AzureStorageEnabled { get; set; }
}
