﻿namespace Microsoft.MessageBrokerArchitecture.Services.EventDestination.Model;

public class CatalogBrand
{
    public int Id { get; set; }

    public string Brand { get; set; }
}
