﻿namespace Microsoft.MessageBrokerArchitecture.Services.EventDestination.Model;

public class CatalogType
{
    public int Id { get; set; }

    public string Type { get; set; }
}
